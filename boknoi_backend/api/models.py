# from django.db import models

from cProfile import label
from pyexpat import model
from unittest import result
from django.views.decorators.csrf import csrf_exempt

import cv2
import base64
import os
import numpy as np
from PIL import Image
import io
from io import TextIOBase
from skimage.measure import ransac
from skimage.transform import ProjectiveTransform, AffineTransform

# ______________________________________ SIFT Ransac ______________________________________
# Create your models here.
# print(cv2.__version__)

## ฟังก์ชันสำหรับอ่านภาพ RGB และ gray-scale 
#   - filename คือ path ของรูปภาพที่ต้องการอ่าน
##  return

def imread(filename):
    rgb_img = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    gray_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2GRAY)

    return rgb_img, gray_img

def read(file):
    decoded_data = base64.b64decode(file)
    np_data = np.frombuffer(decoded_data, dtype=np.uint8)
    rgb_img = cv2.imdecode(np_data, flags=cv2.IMREAD_COLOR)
    gray_img = cv2.imdecode(np_data, flags=cv2.IMREAD_GRAYSCALE)

    return rgb_img, gray_img

def find_kp_desc(gray_componant_im, gray_shop_im, rgb_componant_im):
    # กำหนด feature extractor
    feature_extractor = cv2.SIFT_create()
    # หา keypoint และ descriptor
    kp_componant, desc_componant = feature_extractor.detectAndCompute(gray_componant_im, None)
    kp_shop, desc_shop = feature_extractor.detectAndCompute(gray_shop_im, None)

    return kp_componant, desc_componant, kp_shop, desc_shop

def ransac_match(desc_componant, desc_shop, rgb_componant_im, rgb_shop_im, kp_componant, kp_shop):
  try:
      # กำหนด FLANN parameters
      FLANN_INDEX_KDTREE = 0
      index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
      search_params = dict(checks = 40)
      flann = cv2.FlannBasedMatcher(index_params, search_params)

      # k เป็น 2 เนื่องจากใช้ descriptor ของ componant และ descriptor ของร้าน
      matches = flann.knnMatch(desc_componant, desc_shop, k=2)

      # เก็บค่า keygood matches as per Lowe's ratio test
      good_match = []
      for m,n in matches:
          if m.distance < 0.75*n.distance:
              good_match.append(m)

      # เช็คว่าถ้าจำนวน good_match ที่ได้น้อยกว่า 4 ซึ่งเป็น min_samples ของ RANSAC ให้บอกว่าหาร้านไม่ได้เลย
      if len(good_match) < 4:
          print("not the same shop2")
          return 0
      
      else:
          src_pts = np.float32([kp_componant[m.queryIdx].pt for m in good_match]).reshape(-1, 2)
          dst_pts = np.float32([kp_shop[m.trainIdx].pt for m in good_match]).reshape(-1, 2)

          # ใช้ geometric test ในที่นี้ คือ ransac เพื่อหา keypoint ที่ถูกต้องจริง ๆ (หา inliers ตัดจุดที่ไม่ตรง หรือ outliers ออก )
          model, inliers = ransac(
                      (src_pts, dst_pts),
                      AffineTransform, min_samples=4,
                      residual_threshold=8, max_trials=6000
                  )
          n_inliers = np.sum(inliers)

            # inlier_keypoints_left = [cv2.KeyPoint(point[0], point[1], 1) for point in src_pts[inliers]]
            # inlier_keypoints_right = [cv2.KeyPoint(point[0], point[1], 1) for point in dst_pts[inliers]]
          placeholder_matches = [cv2.DMatch(idx, idx, 1) for idx in range(n_inliers)]

        #   return 1
          if len(placeholder_matches) > 3:
              print("same shop")
              return 1
          else:
              print("not the same shop3")
              return 0 

  except:
      print("not the same shop1")
      return 0

## ฟังก์ชันสำเร็จรวมทุกอย่างตั้งแต่นำรูปเข้าจนถึง ransac เช็คว่าร้านเดียวกันไหม
#  filename1 คือ path ของไฟล์รูปของคอมโพแนน
#  filename2 คือ path ของไฟล์รูปของร้าน

def sift_ransac(rgb_shop_img, gray_shop_img, rgb_com_img, gray_com_img):

    kp_com, desc_com, kp_shop, desc_shop = find_kp_desc(gray_com_img, gray_shop_img, rgb_com_img)

    return ransac_match(desc_com, desc_shop, rgb_com_img, rgb_shop_img, kp_com, kp_shop)


@csrf_exempt
def findStoreBySift(data, picture): 
    rgb_shop_img, gray_shop_img = read(picture)
    check = 0

    for i in range(len(data)):
        check = 0
        print(data[i])
        for j in os.listdir('./api/picture/' + data[i]):
            rgb_com_img, gray_com_img = imread('./api/picture/' + data[i] + '/' + j)
            result = sift_ransac(rgb_shop_img, gray_shop_img, rgb_com_img, gray_com_img)
            if (result == 1):
                check = check + 1
            if (check >= 2):
                return data[i]

    return None

# ______________________________________ YOLO ______________________________________


@csrf_exempt
def findStoreByYolo(picture, models):
    CONFIDENCE = 0.5
    SCORE_THRESHOLD = 0.5
    IOU_THRESHOLD = 0.5

    result = []
    weights_path = []
    labels = []
    colors = []
    net = []

    image, gray = read(picture)
    h_ori, w_ori = image.shape[:2]
    blob = cv2.dnn.blobFromImage(image, 1/255.0, (416, 416), swapRB=True, crop=False)
    
    for i in range(len(models)):
        h = h_ori
        w = w_ori
        config_path = './api/yolov3/' + models[i] + '/yolov3.cfg'
        weights_path = './api/yolov3/' + models[i] + '/yolov3_final.weights'
        labels = open('./api/yolov3/' + models[i] + '/classes.names').read().strip().split('\n')
        colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')
    
        net = cv2.dnn.readNetFromDarknet(config_path, weights_path)
        # sets the blob as the input of the network
        net.setInput(blob)
        # get all the layer names
        ln = net.getLayerNames()
        try:
            ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        except IndexError:
            # in case getUnconnectedOutLayers() returns 1D array when CUDA isn't available
            ln = [ln[i - 1] for i in net.getUnconnectedOutLayers()]
        
        layer_outputs = net.forward(ln)

        font_scale = 1
        thickness = 1
        boxes, confidences, class_ids = [], [], []
        # loop over each of the layer outputs
        for output in layer_outputs:
            # loop over each of the object detections
            for detection in output:
                # extract the class id (label) and confidence (as a probability) of
                # the current object detection
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                # discard out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > CONFIDENCE:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[:4] * np.array([w, h, w, h])
                    # print(box)
                    (centerX, centerY, width, height) = box.astype("int")
                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    # update our list of bounding box coordinates, confidences,
                    # and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        # loop over the indexes we are keeping
        for i in range(len(boxes)):
            # extract the bounding box coordinates
            x, y = boxes[i][0], boxes[i][1]
            w, h = boxes[i][2], boxes[i][3]
            # draw a bounding box rectangle and label on the image
            color = [int(c) for c in colors[class_ids[i]]]
            cv2.rectangle(image, (x, y), (x + w, y + h), color=color, thickness=thickness)
            text = f"{labels[class_ids[i]]}: {confidences[i]:.2f}"
            # calculate text width & height to draw the transparent boxes as background of the text
            (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, fontScale=font_scale, thickness=thickness)[0]
            text_offset_x = x
            text_offset_y = y - 5
            box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height))
            overlay = image.copy()
            cv2.rectangle(overlay, box_coords[0], box_coords[1], color=color, thickness=cv2.FILLED)
            # add opacity (transparency to the box)
            image = cv2.addWeighted(overlay, 0.6, image, 0.4, 0)
            # now put the text (label: confidence %)
            cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=font_scale, color=(0, 0, 0), thickness=thickness)

        # perform the non maximum suppression given the scores defined before
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, SCORE_THRESHOLD, IOU_THRESHOLD)

        DETECTMK=0
        DETECTAMPERSANDGELATO=0
        DETECTCHUAN=0
        DETECTCOCA=0
        DETECTDINTAIFUNG=0
        DETECTEARWTHAI=0
        DETECTIPPUDO=0
        DETECTKATSUGEN=0
        DETECTMAISEN=0
        DETECTMEETFRESH=0
        DETECTMOMOPARADISE=0
        DETECTPEAK=0
        DETECTSEEFAH=0
        DETECTSOUTHTIGER=0
        DETECTSPAGHETTIFACTORY=0
        DETECTSUKISHI=0
        DETECTSWENSENS=0
        DETECTTHEALLEY=0
        DETECTYAYOI=0
        DETECTYOSHINOYA=0

        StoreList = []

        # ensure at least one detection exists
        if len(idxs) > 0:
            # loop over the indexes we are keeping
            for i in idxs.flatten():
                # extract the bounding box coordinates
                x, y = boxes[i][0], boxes[i][1]
                w, h = boxes[i][2], boxes[i][3]
                # draw a bounding box rectangle and label on the image
                color = [int(c) for c in colors[class_ids[i]]]
                cv2.rectangle(image, (x, y), (x + w, y + h), color=color, thickness=thickness)
                # text = f"{labels[class_ids[i]]}"
                text = f"{labels[class_ids[i]]}: {confidences[i]:.2f}"
                # calculate text width & height to draw the transparent boxes as background of the text
                (text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, fontScale=font_scale, thickness=thickness)[0]
                text_offset_x = x
                text_offset_y = y - 5
                box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height))
                overlay = image.copy()
                cv2.rectangle(overlay, box_coords[0], box_coords[1], color=color, thickness=cv2.FILLED)
                # add opacity (transparency to the box)
                image = cv2.addWeighted(overlay, 0.6, image, 0.4, 0)
                # now put the text (label: confidence %)
                cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=font_scale, color=(0, 0, 0), thickness=thickness)

                StoreList = [text];
                # initializing string
                #test_string = "logo-thealley: 1.00"
                # using list comprehension
                # checking if string contains list element
                #res = [ele for ele in StoreList if(ele in test_string)]

                # print result
                #print("Does string contain any list element : " + str(bool(res)))

        AMPERSANDGELATO = 'ampersandgelato'
        for a in StoreList:
            if AMPERSANDGELATO in a:
                DETECTAMPERSANDGELATO=DETECTAMPERSANDGELATO+1

        CHUAN = 'chuan'
        for b in StoreList:
            if CHUAN in b:
                DETECTCHUAN=DETECTCHUAN+1

        COCA = 'coca'
        for c in StoreList:
            if COCA in c:
                DETECTCOCA=DETECTCOCA+1
        
        DINTAIFUNG = 'dintaifung'
        for d in StoreList:
            if DINTAIFUNG in d:
                DETECTDINTAIFUNG=DETECTDINTAIFUNG+1

        IPPUDO = 'ippudo'
        for e in StoreList:
            if IPPUDO in e:
                DETECTIPPUDO=DETECTIPPUDO+1

        KATSUGEN = 'katsugen'
        for f in StoreList:
            if KATSUGEN in f:
                DETECTKATSUGEN=DETECTKATSUGEN+1

        MAISEN = 'maisen'
        for g in StoreList:
            if MAISEN in g:
                DETECTMAISEN=DETECTMAISEN+1

        MEETFRESH = 'meetfresh'
        for h in StoreList:
            if MEETFRESH in h:
                DETECTMEETFRESH=DETECTMEETFRESH+1

        MK = 'mk'
        for i in StoreList:
            if MK in i:
                DETECTMK=DETECTMK+1

        MOMOPARADISE = 'momoparadise'
        for j in StoreList:
            if MOMOPARADISE in j:
                DETECTMOMOPARADISE=DETECTMOMOPARADISE+1

        PEAK = 'peak'
        for k in StoreList:
            if PEAK in k:
                DETECTPEAK=DETECTPEAK+1

        SEEFAH = 'seefah'
        for l in StoreList:
            if SEEFAH in l:
                DETECTSEEFAH=DETECTSEEFAH+1

        SOUTHTIGER = 'southtiger'
        for m in StoreList:
            if SOUTHTIGER in m:
                DETECTSOUTHTIGER=DETECTSOUTHTIGER+1

        SPAGHETTIFACTORY = 'spaghettifactory'
        for n in StoreList:
            if SPAGHETTIFACTORY in n:
                DETECTSPAGHETTIFACTORY=DETECTSPAGHETTIFACTORY+1

        SUKISHI = 'sukishi'
        for o in StoreList:
            if SUKISHI in o:
                DETECTSUKISHI=DETECTSUKISHI+1

        SWENSENS = 'swensens'
        for p in StoreList:
            if SWENSENS in p:
                DETECTSWENSENS=DETECTSWENSENS+1
        
        EARWTHAI = 'earwthai'
        for q in StoreList:
            if EARWTHAI in q:
                DETECTEARWTHAI=DETECTEARWTHAI+1

        THEALLEY = 'thealley'
        for r in StoreList:
            if THEALLEY in r:
                DETECTTHEALLEY=DETECTTHEALLEY+1
        
        YAYOI = 'yayoi'
        for s in StoreList:
            if YAYOI in s:
                DETECTYAYOI=DETECTYAYOI+1

        YOSHINOYA = 'yoshinoya'
        for t in StoreList:
            if YOSHINOYA in t:
                DETECTYOSHINOYA=DETECTYOSHINOYA+1  

        StoreDetectCount = {'AMPERSANDGELATO': DETECTAMPERSANDGELATO, 'CHUAN': DETECTCHUAN, 'COCA': DETECTCOCA, 'DINTAIFUNG': DETECTDINTAIFUNG,
                    'IPPUDO': DETECTIPPUDO, 'KATSUGEN': DETECTKATSUGEN, 'MAISEN': DETECTMAISEN, 'MEETFRESH': DETECTMEETFRESH,
                    'MK': DETECTMK, 'MOMOPARADISE': DETECTMOMOPARADISE, 'PEAK': DETECTPEAK, 'SEEFAH': DETECTSEEFAH,
                    'SOUTHTIGER': DETECTSOUTHTIGER, 'SPAGHETTIFACTORY': DETECTSPAGHETTIFACTORY, 'SUKISHI': DETECTSUKISHI, 'SWENSENS': DETECTSWENSENS,
                    'EARWTHAI': DETECTEARWTHAI, 'THEALLEY': DETECTTHEALLEY, 'YAYOI': DETECTYAYOI, 'YOSHINOYA': DETECTYOSHINOYA}
    
        if len(set(StoreDetectCount.values())) == 1:
          print("Can't detect Store")
        else:
          DetectedStore = max(StoreDetectCount, key=StoreDetectCount.get)
          print("This store is:",DetectedStore)
          print(StoreList)
          result.append(DetectedStore)

    return result