from ast import Store
from email import message
from unicodedata import name
from unittest import result
from django.shortcuts import render
from django.http import HttpResponse, response
from django.views.decorators.csrf import csrf_exempt

import os, csv, json
# from pprint import pprint

from api.LineMessage import LineMessage
# from api.models import findStoreBySift
from api.models import findStoreByYolo

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("./api/serviceAccountKey.json")
firebase_admin.initialize_app(cred)

db = firestore.client()

# Create your views here.

# Recieving request
@csrf_exempt
def findStoreByName(request):
    # 1. recieve GPS and name
    body = request.body.decode('utf-8') 
    data = json.loads(body)
    data = data['payload']['payload']
    
    # 2. find Store
    docs1 = db.collection(u'Stores').where(u'cname', u'array_contains', data['name'].lower()).where(
        u'gps.latitude', u'>', data['gps']['latitude']-0.0003).where(
            u'gps.latitude', u'<', data['gps']['latitude']+0.0003).get()
 
    docs2 = db.collection(u'Stores').where(u'cname', u'array_contains', data['name'].lower()).where(
        u'gps.longitude', u'>', data['gps']['longitude']-0.0003).where(
            u'gps.longitude', u'<', data['gps']['longitude']+0.0003).get()

    data['store'] = []
    data['detail'] = []
    temp = {'name': [], 'detail': []}

    for doc in docs1:
        temp['name'].append(doc.id)
        temp['detail'].append(doc.to_dict())

    for doc in docs2:
        if (doc.id in temp['name']):
            data['store'].append(doc.id)
            data['detail'].append(doc.to_dict())

    # 3. send data to ALGORITHM
    if (data['store']):
        # send all store that same store but difference floor
        for i in range(len(data['store'])):
            # prepare working hour for messages
            if (len(data['detail'][i]['hours']) > 1):
                data['detail'][i]['hour'] = "\nเวลาเปิดให้บริการ \nวันจันทร์ " + data['detail'][i]['hours']['monday'] + "\nวันอังคาร " + data['detail'][i]['hours']['tuesday'] + "\nวันพุธ " + data['detail'][i]['hours']['wednesday'] + "\nวันพฤหัสบดี " + data['detail'][i]['hours']['thursday'] + "\nวันศุกร์ " + data['detail'][i]['hours']['friday'] + "\nวันเสาร์ " + data['detail'][i]['hours']['saturday'] + "\nวันอาทิตย์ " + data['detail'][i]['hours']['sunday']
            else:
                data['detail'][i]['hour'] = "เปิดให้บริการทุกวันเวลา " + data['detail'][0]['hours']['everyday']

            # prepare menu info for message
            data['detail'][i]['menu'] = ""
            if ('promotion' in data['detail'][i]['menus']):
                data['detail'][i]['menu'] = "\nโปรโมชัน\n"
                if (len(data['detail'][i]['menus']['promotion']) > 3):
                    for j in range(0, 3):
                        data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"
                else:
                    for j in range(len(data['detail'][i]['menus']['promotion'])):
                        data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"

            if ('recommendation' in data['detail'][i]['menus']):
                data['detail'][i]['menu'] = data['detail'][i]['menu'] + "\nเมนูแนะนำ\n"
                if (len(data['detail'][i]['menus']['recommendation']) > 4):
                    for j in range(0, 3):
                        data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"
                else:
                    for j in range(len(data['detail'][i]['menus']['recommendation'])):
                        data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"

            lineMessageFound1(data['id'], data['detail'][i])
            lineMessageFound2(data['id'], data['detail'][i])
    else:
        lineMessageNotFound(data['id'])

    # print("find name")
    return HttpResponse('success')

@csrf_exempt
def findStoreByPicture(request):
    body = request.body.decode('utf-8') 
    data = json.loads(body)
    data = data['payload']['payload']

    # 1. Recieve GPS and picture then filter store by using GPS
    # Using with YOLO
    docs1 = db.collection(u'Models').where(u'latitude', u'>', data['gps']['latitude']-0.00015).where(
        u'latitude', u'<', data['gps']['latitude']+0.00015).get()

        
    docs2 = db.collection(u'Models').where(u'longitude', u'>', data['gps']['longitude']-0.00015).where(
        u'longitude', u'<', data['gps']['longitude']+0.00015).get()

    # Using with SIFT
    # docs1 = db.collection(u'Stores').where(u'gps.latitude', u'>', data['gps']['latitude']-0.00015).where(
    #     u'gps.latitude', u'<', data['gps']['latitude']+0.00015).get()

    # docs2 = db.collection(u'Stores').where(u'gps.longitude', u'>', data['gps']['longitude']-0.00015).where(
    #     u'gps.longitude', u'<', data['gps']['longitude']+0.00015).get()

    data['model'] = []
    data['store'] = []
    data['detail'] = []
    temp = {'name': [], 'detail': [], 'model':[]}

    # YOLO
    for doc in docs1:
        temp['model'].append(doc.id)

    for doc in docs2:
        if (doc.id not in temp['model']):
            data['model'].append(doc.id)

    # SIFT
    # for doc in docs1:
    #     temp['name'].append(doc.id)
    #     temp['detail'].append(doc.to_dict())

    # for doc in docs2:
    #     if (doc.id in temp['name']):
    #         data['store'].append(doc.id)
    #         data['detail'].append(doc.to_dict())

    # 2. find store by Algorithm
    # YOLO
    result = findStoreByYolo(data['url'].split(',')[1], data['model'])
    # SIFT
    # store = findStoreBySift(data['store'], data['url'].split(',')[1])

    # 3. send message to LINE

    if (result):
    # SIFT
    # if (data['store']):
    #     print(data['store'])

    #     if(store == None):
    #         lineMessageNotFound(data['id'])
    #     else:

    # YOLO
        for i in range(len(result)):
            docs3 = db.collection(u'Stores').where(u'cname', u'array_contains', result[i].lower()).get()
            for doc in docs3:
                data['store'].append(doc.id)
                data['detail'].append(doc.to_dict())
                if (data['store'][i]):
                    # prepare working hour for messages
                    if (len(data['detail'][i]['hours']) > 1):
                        data['detail'][i]['hour'] = "\nเวลาเปิดให้บริการ \nวันจันทร์ " + data['detail'][i]['hours']['monday'] + "\nวันอังคาร " + data['detail'][i]['hours']['tuesday'] + "\nวันพุธ " + data['detail'][i]['hours']['wednesday'] + "\nวันพฤหัสบดี " + data['detail'][i]['hours']['thursday'] + "\nวันศุกร์ " + data['detail'][i]['hours']['friday'] + "\nวันเสาร์ " + data['detail'][i]['hours']['saturday'] + "\nวันอาทิตย์ " + data['detail'][i]['hours']['sunday']
                    else:
                        data['detail'][i]['hour'] = "เปิดให้บริการทุกวันเวลา " + data['detail'][0]['hours']['everyday']

                    # prepare menu info for message
                    data['detail'][i]['menu'] = ""
                    if ('promotion' in data['detail'][i]['menus']):
                        data['detail'][i]['menu'] = "\nโปรโมชัน\n"
                        if (len(data['detail'][i]['menus']['promotion']) > 3):
                            for j in range(0, 3):
                                data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"
                        else:
                            for j in range(len(data['detail'][i]['menus']['promotion'])):
                                data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"

                    if ('recommendation' in data['detail'][i]['menus']):
                        data['detail'][i]['menu'] = data['detail'][i]['menu'] + "\nเมนูแนะนำ\n"
                        if (len(data['detail'][i]['menus']['recommendation']) > 4):
                            for j in range(0, 3):
                                data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"
                        else:
                            for j in range(len(data['detail'][i]['menus']['recommendation'])):
                                data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"

                    lineMessageFound1(data['id'], data['detail'][i])
                    lineMessageFound2(data['id'], data['detail'][i])
                else:
                    lineMessageNotFound(data['id'])

    # SIFT
    #         for i in range(len(data['store'])):
    #             if (data['store'][i] == store):
    #                 # prepare working hour for messages
    #                 if (len(data['detail'][i]['hours']) > 1):
    #                     data['detail'][i]['hour'] = "\nเวลาเปิดให้บริการ \nวันจันทร์ " + data['detail'][i]['hours']['monday'] + "\nวันอังคาร " + data['detail'][i]['hours']['tuesday'] + "\nวันพุธ " + data['detail'][i]['hours']['wednesday'] + "\nวันพฤหัสบดี " + data['detail'][i]['hours']['thursday'] + "\nวันศุกร์ " + data['detail'][i]['hours']['friday'] + "\nวันเสาร์ " + data['detail'][i]['hours']['saturday'] + "\nวันอาทิตย์ " + data['detail'][i]['hours']['sunday']
    #                 else:
    #                     data['detail'][i]['hour'] = "เปิดให้บริการทุกวันเวลา " + data['detail'][0]['hours']['everyday']

    #                 # prepare menu info for message
    #                 data['detail'][i]['menu'] = ""
    #                 if ('promotion' in data['detail'][i]['menus']):
    #                     data['detail'][i]['menu'] = "\nโปรโมชัน\n"
    #                     if (len(data['detail'][i]['menus']['promotion']) > 3):
    #                         for j in range(0, 3):
    #                             data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"
    #                     else:
    #                         for j in range(len(data['detail'][i]['menus']['promotion'])):
    #                             data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['promotion'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['promotion'][j]['price'])) + " บาท\n"

    #                 if ('recommendation' in data['detail'][i]['menus']):
    #                     data['detail'][i]['menu'] = data['detail'][i]['menu'] + "\nเมนูแนะนำ\n"
    #                     if (len(data['detail'][i]['menus']['recommendation']) > 4):
    #                         for j in range(0, 3):
    #                             data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"
    #                     else:
    #                         for j in range(len(data['detail'][i]['menus']['recommendation'])):
    #                             data['detail'][i]['menu'] = data['detail'][i]['menu'] + data['detail'][i]['menus']['recommendation'][j]['name'] + " \t\t " + str(int(data['detail'][i]['menus']['recommendation'][j]['price'])) + " บาท\n"

    #                 lineMessageFound1(data['id'], data['detail'][i])
    #                 lineMessageFound2(data['id'], data['detail'][i])

    else:
        lineMessageNotFound(data['id'])

    return HttpResponse('success')

# Sending store detail
def getStore(request):
    print(request.GET.get('id'))

    # 1. recieve param (store name[id])
    id = request.GET.get('id')

    # 2. get store from DB
    doc = db.collection(u'Stores').document(id).get()
    data = doc.to_dict()

    print(type(data))
    data = json.dumps(data)
    print(type(data))

    # 3. send data
    return HttpResponse(f'{ data }')

# Sending LINE message
@csrf_exempt
def lineMessageNotFound(request):
    # required 'user token id'
    reply_token = request

    messages = [
        {
            "type": "text",
            "text": "ค้นหาร้านค้าไม่พบ\n\nโปรดลองใหม่อีกครั้ง"        
        }
    ]

    # print(reply_token, messages)
    line_message = LineMessage(messages)
    line_message.reply(reply_token)
    
    return HttpResponse('success')

@csrf_exempt
def lineMessageFound1(request, detail):
    # required 'user token id'
    reply_token = request
    print(reply_token)

    messages = [
        {
            "type": "text",
            "text": "นี่คือร้าน " + detail['name'] + "\n" + detail['category'] + "\n" + detail['hour'] + "\n" + detail['menu']
        }
    ]

    line_message = LineMessage(messages)
    line_message.reply(reply_token)
    
    return HttpResponse('success')
    

@csrf_exempt
def lineMessageFound2(request, detail):
    # required 'user token id'
    reply_token = request
    
    url = "https://boknoi-343409.web.app/store/" + detail['id']
    messages = [
        {
            "type": "flex",
            "altText": "พบร้านที่ค้นหาแล้ว มาดูกันเลย",
            "contents": {
                "type": "bubble",
                "size": "giga",
                "direction": "ltr",
                "action": {
                    "type": "uri",
                    "label": "รายละเอียดร้านค้า",
                    "uri": url
                },
                "header": {
                    "type": "box",
                    "layout": "vertical",
                    "contents": [
                        {
                            "type": "text",

                            "text": detail['name'], 

                            "weight": "bold",
                            "size": "xxl",
                            "color": "#1B1B1BFF",
                            "align": "center",
                            "gravity": "top",
                            "margin": "none",
                            "wrap": True,

                            "action": {
                            "type": "uri",
                            "uri": url
                            },

                            "contents": []
                        }
                    ]
                },
                "hero": {
                    "type": "image",

                    "url": detail['background'],

                    "size": "full",
                    "aspectRatio": "1.51:1",
                    "aspectMode": "cover",
                    "backgroundColor": "#FFFFFFFF"
                },
                "footer": {
                    "type": "box",
                    "layout": "horizontal",
                    "contents": [
                            {
                                "type": "button",
                                "action": {
                                "type": "uri",
                                "label": "ดูข้อมูลเพิ่มเติม",

                                "uri": url
                                },
                            }
                        ]
                }
            }
        }
    ]

    line_message = LineMessage(messages)
    line_message.reply(reply_token)
    
    return HttpResponse('success')

@csrf_exempt
def addStore(request): 
    body = request.body.decode('utf-8') 
    data = json.loads(body)
    data = data['payload']['payload']

    # Create ID
    data['id'] = data['name'].split(' ')
    data['id'] = ''.join(data['id'])
    print(data['id'])

    # Upload to firestore DB
    db.collection(u'Stores').document(data['id']).set(data)

    return HttpResponse('success')

# -------------------- TEST --------------------

def create(request):
    with open('./api/stores/result.json', encoding="utf8") as json_data:
        docs = json.load(json_data)

    for i in range(len(docs)):
        id = docs[i]['id']
        db.collection(u'Stores').document(id).set(docs[i])

    return HttpResponse('success')

