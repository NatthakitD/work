import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Upload from '../views/UploadStore.vue'
import Store from '../views/StoreInfo.vue'
// import Home from '../views/Home_Video.vue' // video

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/store/:id',
    name: 'Store',
    component: Store
  },
  {
    path: '/upload',
    name: 'Upload',
    component: Upload
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
