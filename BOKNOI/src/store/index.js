import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    UPDATE_FIND_STORE: false,
    REPLY_STATUS: false,
    REPLY_ERROR: false,
    UPDATE_ADD_STORE: '',
    data: {
      background: '',
      recommendation: '',
      storeName: '',
      category: '',
      workingHour: '',
      about: '',
      contact: {
        facebook: '',
        line: '',
        phone: '',
        website: '',
      },
      menus: ''
    }
  },
  getters: {
    loadingStatus (state) {
      return state.UPDATE_FIND_STORE;
    },
    replied (state) {
      return state.REPLY_STATUS;
    },
    errorReplied (state) {
      return state.REPLY_ERROR
    }
  },
  mutations: {
    addStore(state, data) {
      state.UPDATE_ADD_STORE = data
    },
    updateFindStore(state, data) {
      state.UPDATE_FIND_STORE = data
    },
    updateMassageStatus(state, data) {
      state.REPLY_STATUS = data
    },
    updateMessageError(state, data) {
      state.REPLY_ERROR = data
    },
    setStore(state, data) {
      console.log('raw data: ', data)
      state.data.background = data.background
      state.data.recommendation = data.recommendation
      state.data.storeName = data.name
      state.data.category = data.category
      state.data.about = data.description
      state.data.workingHour = data.hours
      state.data.contact = data.contact
      state.data.menus = data.menus
      console.log('mutation: ', state.data)
    }
  },

  actions: {
    findStoreByName({ commit }, payload) {
      console.log(payload.payload)
      commit('updateFindStore', true)
      console.log('find by Name')
      axios.post('https://boknoi-backend-k6yrbjd72a-as.a.run.app/api/findStoreByName', {
        payload
      })
      .then(response => {
        commit('updateFindStore', false)
        commit('updateMassageStatus', true)
        console.log(response)
      })
      .catch(function (error) {
        commit('updateMessageError', true)
        console.log(error)
      })
    },

    findStoreByPicture({ commit }, payload) {
      console.log(payload.payload)
      commit('updateFindStore', true)
      console.log('find by Picture')
      axios.post('https://boknoi-backend-k6yrbjd72a-as.a.run.app/api/findStoreByPicture', {
        payload
      })
      .then(response => {
        commit('updateFindStore', false)
        commit('updateMassageStatus', true)
        console.log(response)
      })
      .catch(function (error) {
        commit('updateMessageError', true)
        console.log(error)
      })
    },

    async getStore({ commit }, id) {
      console.log('action: ' + id)
      await axios.get('https://boknoi-backend-k6yrbjd72a-as.a.run.app/api/getStore/?id=' + id)
      .then((response) => {
        commit('setStore', response.data)
      })
      .catch(function (error) {
        console.log(error);
      })
    },

    async addStore({ commit }, payload) {
      console.log(payload.payload)
      await axios.post('https://boknoi-backend-k6yrbjd72a-as.a.run.app/api/addStore', {
        payload
      })
      .then((response) => {
        commit('addStore', response)
        console.log(response)
      })
      .catch(function (error) {
        console.log(error);
      })
    }
  },

  modules: {
  }
})
