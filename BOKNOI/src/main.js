import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Datepicker from '@vuepic/vue-datepicker'
import '@vuepic/vue-datepicker/dist/main.css'
import { initializeApp } from 'firebase/app';
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// createApp(App).use(store).use(router).mount('#app')

const boknoi = createApp(App)

boknoi.component('Datepicker', Datepicker);

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDlpR7XLsyWC50eizFKIkMPsl0gg_aZ41Y",
  authDomain: "boknoi-343409.firebaseapp.com",
  projectId: "boknoi-343409",
  storageBucket: "boknoi-343409.appspot.com",
  messagingSenderId: "531982912370",
  appId: "1:531982912370:web:798db1e5f0b5535a30ebeb"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

boknoi
.use(firebaseApp)
.use(store)
.use(router)

boknoi.mount('#app')