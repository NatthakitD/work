library(ggplot2)
library(tidyverse)
library(tidyr)
library(dplyr)
library(corrplot)

insure <- read.csv("insurance.csv")

#_______________________________________________________________

insure %>% 
  ggplot() +
  geom_point(aes(x = age, y = charges))
 
cor(insure$age, insure$charges)

#______________________________________________________________

insure %>%
  mutate(age_range = cut(age,
                         breaks = c(-Inf,20,30,40,50,60,Inf),
                         labels = c("<20","20-29","30-39",
                                    "40-49","50-59",">60"))) -> IS

table(IS$age_range, IS$smoker) -> IS1
cbind(IS1, Total = rowSums(IS1)) -> IS1
rbind(IS1, Total = colSums(IS1)) -> IS1
IS1

chisq.test(table(IS$age_range, IS$smoker))

IS %>% 
  ggplot() +
  geom_bar(aes(x = age_range, fill = smoker), position = 'fill')





