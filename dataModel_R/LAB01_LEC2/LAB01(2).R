x <- 1:10
x[2:4] <- NULL
x <- length(x)
x

y <- c(TRUE, 5, 'a')
y1 <- c(TRUE, FALSE)
y2 <- c(TRUE, 1, FALSE)

#try this
1+1
mean(runif(100))
1:10
2**10
abs(rnorm(10))
rnorm(10)
x <- c(1,2,4)
q <- c(x,x,8)
q[1]
q[1:4]
q[2:3]
q[-2]
q[-2:-3]
q[c(2,6)]

s <- mean(x)
s1 <- sd(x)
s1

data(Nile)
Nile
hist(Nile)

oddcount <- function(x) {
  k <- 0
  for (n in x) {
    if (n %% 2 == 1) k <- k+1
  }
  return(k)
}

oddcount(2:6)
oddcount(c(2,3,5,7,8,9))

x1 <- 'hello' # 'x'  ==  "x"
x2 <- 5.5:7
x3 <- 1.5:3.5
x4 <- 1.5
x5 <- as.integer(2)
x6 <- as.integer(2:4)
x7 <- 1+1i
x8 <- 1i+2i
x9 <- FALSE
x10 <- as.Date(0, origin = "2000-03-24")
x11 <- c(x1[2],5)
x12 <- c(x1, 3, TRUE)
x13 <- c(x3[-1],NA,0)
x14 <- c(1i, "hi", 2+2)
x15 <- c(1i, TRUE)

class(x1)  #chaeck class of variable

length(x6)
z <- c(1,1,1,1)
z + c(1,2)
z + 1:2
z + 2
z - 2*2**2
z - c(1+1,2+2)/2

z1 <- c(z[1:2],NA,0,0)
z2 <- c(z[1:2],NULL,0,0)
mean(x[-2])
mean(z1)
mean(z1, na.rm = TRUE)  #na.rm == na.omit
mean(z2)
z <- NULL
z
z2 >= 1
z2[z2==0] <- 2
z2
which(z2**2==4)

#Matrix
m <- matrix(1:4, nrow = 2, ncol = 2)
m1 <- matrix(c(NA,0,1,2), nrow = 2, ncol = 2)
m1
m
m2 <- matrix(1:8, ncol = 4, byrow =  TRUE)
m2
m2[,2:3]
m2[2,]
m2[2,2:3]  #[row, col]

#List
j <- list(name = "KK", no = 001, like = TRUE)
j
j$no
class(j$no)
j$like
class(j$like)

#Data frame
kid <- c("JJ", "LL")
age <- c(2, 6)
d <- data.frame(kid,age)
d
d[1]
d[[1]]
d[1,]
d[,1]
d[1,1]
d[[2]]
d[2,]   
str(d)

mtcars
head(mtcars)
tail(mtcars)

