library(ggplot2)
library(tidyverse)
library(tidyr)
library(dplyr)

KickStarter <- read.csv("Kickstarter_ 2017.csv")

#��á�Ш�¢ͧ��������ع�����������ѡ����д��ع����稵���������
KickStarter %>% 
  group_by(main_category, state) %>% 
  filter(state == 'successful') %>% 
  summarise(n = n()) %>% 
  ggplot() + 
  geom_col(aes(x = main_category, y = n, fill = main_category))

#��á�Ш�¢ͧ ����д��ع�Թ �������� �������稪�ǧ 0 - 2,000,000$  
KickStarter %>%
  filter(state == 'successful', main_category == 'Games', usd.pledged <= 1000000) %>% 
  ggplot() + 
  geom_histogram(aes(x = usd.pledged), color = "black", bins = 50)


KickStarter %>%
  filter(state == 'successful', main_category == 'Games', usd.pledged <= 1000000) %>% 
  summarise(n = n(),
            Q1 = quantile(usd.pledged,0.25),
            mean = mean(usd.pledged),
            median = median(usd.pledged),
            Q3 = quantile(usd.pledged,0.75),
            IQR = IQR(usd.pledged),
            min = min(usd.pledged),
            max = max(usd.pledged))

  

